import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';

import { BannerProvider } from './../../providers/banner/banner';
import { UserProvider } from './../../providers/user/user';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
  ],
  providers: [
    UserProvider,
    BannerProvider
  ]
})
export class LoginPageModule {}
