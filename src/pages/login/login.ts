import { BannerProvider } from './../../providers/banner/banner';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { Constants } from './../../util/constants';
import { DialogComponent } from './../../components/dialog/dialog';
import { LabelProperties } from './../../util/labels-properties';
import { UserProvider } from './../../providers/user/user';
import User from '../../models/user/user.model';
import Auth from '../../models/user/auth.model';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user: User = new User()

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public labels: LabelProperties,
    private constants: Constants,
    private userProvider: UserProvider,
    private dialog: DialogComponent,
    private storage: Storage,
    private anuncio: BannerProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad: ionViewDidLoad')

    this.anuncio.managerBanner()
    this.anuncio.managerInterstitial()
  }

  ionViewWillLeave() {
    console.log('cancel interval banner: HomePage')
    this.anuncio.cancelInterstitialInterval()
  }

  login() {
    if (this.user && this.user.email && this.user.password) {
      this.dialog.loading()
      this.userProvider.login(this.user).subscribe((data: Auth) => {
        this.dialog.hide()
        if (data.user) {
          this.storage.set(this.constants.DT_USER, data.user)
          this.storage.set(this.constants.DT_TOKEN, data.token)

          this.navCtrl.setRoot('HomePage')
        }
      }, err => {
        this.dialog.hide()
        this.dialog.alertToast(this.labels.MRQ_0001)
      })
    } else {
      this.dialog.alert(this.labels.MFR_0001)
    }
  }

}
