import { OutlayProvider } from './../../providers/outlay/outlay';
import { BannerProvider } from './../../providers/banner/banner';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Constants } from './../../util/constants';
import User from '../../models/user/user.model';
import Outlay from '../../models/outlay/outlay.model';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  private token: string = ''
  public user: User
  public outForm: Outlay = new Outlay
  public outlays: Outlay[] = []
  private pageScroll: number = 0

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private constants: Constants,
    private anuncio: BannerProvider,
    private outlayProvider: OutlayProvider
  ) {
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter: HomePage')
    
    if (!this.user) {
      this.storage.get(this.constants.DT_USER).then((data: User) => { this.user = data })
    }

    if(!this.token) {
      this.storage.get(this.constants.DT_TOKEN).then(data => { this.token = data })
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad: HomePage')
    
    // this.anuncio.managerBanner()
    // this.anuncio.managerInterstitial()
    setTimeout(() => {
      this.list(null)
    }, 500);
  }

  ionViewWillLeave() {
    console.log('cancel interval banner: HomePage')
    // this.anuncio.cancelInterstitialInterval()
  }

  listMore(infiniteScroll?) {
    console.log(1)
    this.pageScroll++
    this.list(infiniteScroll)
  }

  list(infiniteScroll?) {
    if (this.user && this.token) {
      this.outlayProvider.list(this.token, this.user._id, this.pageScroll).subscribe((data: Outlay[]) => {

        if (data && data.length) {
          if (this.outlays && this.outlays.length) {
            for (let i in data) {
              this.outlays.push(data[i])
            }
          } else {
            this.outlays = data
          }
        } else if(this.pageScroll > 0) {
          this.pageScroll--
        }

        if (infiniteScroll) infiniteScroll.complete()
      })
    }
  }

  formatOutlay(e) {
    let val: string = e.target.value
    val = val.replace(/\D/g, "") //Remove tudo o que não é dígito
    val = val.replace(/(\d)(\d{2})$/, "$1,$2") //Coloca virgula antes dos 2 últimos numeros
    if (e.type === 'blur') {
      val = val.length === 2 ? '00,' + val : val
      this.outForm.value = val
    }
    e.target.value = val
  }

  createOut() {
    if (this.outForm.value) {
      this.outForm.value = typeof parseInt(this.outForm.value.toString()) == "number" ? parseInt(this.outForm.value.toString()) : 0
      this.outForm._user = this.user._id
      this.outlayProvider.create(this.token, this.outForm).subscribe((data: Outlay) => {
        if (data) {
          this.outlays.unshift(data)
        }
      })
    }
  }

}
