import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { ExheaderComponent } from '../../components/exheader/exheader';
import { ExfabComponent } from './../../components/exfab/exfab';
import { BannerProvider } from './../../providers/banner/banner';
import { OutlayProvider } from '../../providers/outlay/outlay';

@NgModule({
  declarations: [
    HomePage,
    ExheaderComponent,
    ExfabComponent
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
  ],
  providers: [
    BannerProvider,
    OutlayProvider
  ]
})
export class HomePageModule {}
