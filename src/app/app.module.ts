import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AdMobFree } from '@ionic-native/admob-free';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';

import { Constants } from './../util/constants';
import { ConstantsURL } from './../util/constants-url';
import { LabelProperties } from '../util/labels-properties';
import { DialogComponent } from './../components/dialog/dialog';
import { UserProvider } from '../providers/user/user';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__meusgastosdb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AdMobFree,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Constants,
    ConstantsURL,
    LabelProperties,
    DialogComponent,
    UserProvider,
  ]
})
export class AppModule {}
