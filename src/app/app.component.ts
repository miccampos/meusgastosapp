import { Constants } from './../util/constants';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import User from '../models/user/user.model';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = ''
  user: User
  pages: Array<{title: string, component: any}>

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private constants: Constants,
    private storage: Storage
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage' },
    ];

  }

  private initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleBlackOpaque()
      this.statusBar.backgroundColorByHexString('#712c30')
      // this.statusBar.overlaysWebView(false)

      this.splashScreen.hide()
      this.verifyUserAuth()
      // this.rootPage = 'HomePage'
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  private async verifyUserAuth() {
    this.user = await this.storage.get(this.constants.DT_USER)
    let token = await this.storage.get(this.constants.DT_TOKEN)
    if (this.user && token) this.rootPage = 'HomePage'
    else this.rootPage = 'LoginPage'
  }

  async logout() {
    await this.storage.clear()
    this.rootPage = 'LoginPage'
  }
}
