export default class User {
    _id?: string
    name?: string
    email: string
    password?: string
    statistics?: any
    created?: string
    updated?: string
}