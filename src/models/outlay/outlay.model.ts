export default class Outlay {
    _id?: string
    _user: string | any
    _category?: string | any
    value: string | number
    description?: string
    created?: string
    updated?: string
}