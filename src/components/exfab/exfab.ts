import { Component, Input, ElementRef, Renderer } from '@angular/core'

@Component({
  selector: 'exfab',
  templateUrl: 'exfab.html'
})
export class ExfabComponent {

  @Input('scrollArea') scrollArea: any
  @Input('fabPositionBottom') fabPositionBottom: number
  private fab: any
  
  constructor(public element: ElementRef, public renderer: Renderer) {
  }

  ngOnInit() {
    this.fab = this.element.nativeElement.children[0]
    this.renderer.setElementStyle(this.fab, 'bottom', this.fabPositionBottom + 'px')

    this.scrollArea.ionScroll.subscribe((ev) => {
      this.setFabPosition(ev)
    });
  }

  setFabPosition(ev) {
    ev.domWrite(() => {
      let toUp = this.fabPositionBottom + ev.scrollTop
      if (ev.scrollTop > 0) {
        if (toUp < 10) this.renderer.setElementStyle(this.fab, 'bottom', toUp + 'px')
        else this.renderer.setElementStyle(this.fab, 'bottom', '10px')
      }
    })
  }
}
