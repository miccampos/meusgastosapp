import { NgModule } from '@angular/core';
import { DialogComponent } from './dialog/dialog';
import { ExheaderComponent } from './exheader/exheader';
import { ExfabComponent } from './exfab/exfab';
@NgModule({
	declarations: [DialogComponent,
    ExheaderComponent,
    ExfabComponent],
	imports: [],
	exports: [DialogComponent,
    ExheaderComponent,
    ExfabComponent]
})
export class ComponentsModule {}
