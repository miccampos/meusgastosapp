import { Component, Injectable } from '@angular/core';
import { AlertController, LoadingController, Loading, ToastController } from 'ionic-angular';

@Component({
  selector: 'dialog',
  templateUrl: 'dialog.html'
})

@Injectable()
export class DialogComponent {

  private load: Loading

  constructor(
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController
  ) {
  }

  loading() {
    this.load = this.loadingCtrl.create({
      content: 'Aguarde...',
      spinner: 'dots',
      dismissOnPageChange: true
    })
    this.load.present()
  }

  alert(title: string, message: string = null, next = null) {
    let alert = this.alertCtrl.create()
    alert.setTitle(title)
    if (message) alert.setMessage(message)
    if (next) alert.addButton({ text: 'Ok', handler: () => next() })
    else alert.addButton('Ok')
    alert.present()
  }

  alertToast(message: string) {
    this.toastCtrl.create({
      message: message,
      duration: 4000,
      position: 'bottom'
    })
    .present()
  }

  hide() { this.load.dismiss() }

}
