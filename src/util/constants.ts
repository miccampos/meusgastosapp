import { Injectable } from '@angular/core';

@Injectable()
export class Constants {
    // Storage data
    readonly DT_USER: string = '_user'
    readonly DT_TOKEN: string = '_token'

    // Quantidade de items exibidos por vez na pagina home
    readonly QTT_ITEMS_OUTLAY: number = 10

    // Tempo de recorrencia que o banner aparece (3min)
    readonly BANNER_SHOW_TIMER: number = 1000 * 60 * 3
}