import { Injectable } from '@angular/core'

@Injectable()
export class ConstantsURL {

    // private readonly API_VERSION: string = '/v1.0'
    private readonly API_DEV: string = 'http://192.168.0.105:3000'
    // private readonly API_HML: string = ''
    // private readonly API_PROD: string = ''
    private readonly API_URL: string = `${this.API_DEV}`

    // URLS
    readonly ENDPOINT_USER_LOGIN = `${this.API_URL}/users/login`
    readonly ENDPOINT_USER_CREATE = `${this.API_URL}/users/create`

    readonly ENDPOINT_OUTLAY_GET = `${this.API_URL}/outlay`
    readonly ENDPOINT_OUTLAY_CREATE = `${this.API_URL}/outlay/create`
    readonly ENDPOINT_OUTLAY_UPDATE = `${this.API_URL}/outlay/update`
    readonly ENDPOINT_OUTLAY_DELETE = `${this.API_URL}/outlay/delete`
}