import { Injectable } from '@angular/core';

@Injectable()
export class LabelProperties {

    readonly ACCESS: string = 'Acessar'
    readonly FORGET_MY_PASS: string = 'Esqueci minha senha'
    readonly FIRST_ACCESS: string = 'Primeiro acesso'

    readonly MRQ_0001: string = 'Houve um problema ao processar a sua requisição. Por favor, tente novamente.'
    readonly MFR_0001: string = 'Informações inseridas inválidas.'
}