import { Constants } from './../../util/constants';
import { Injectable } from '@angular/core';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';

@Injectable()
export class BannerProvider {

  private readonly bannerConfig: AdMobFreeBannerConfig = {
    id: 'ca-app-pub-2882779297547171/6920033085',
    isTesting: true,
    autoShow: true,
  }

  private readonly interstitialConfig: AdMobFreeInterstitialConfig = {
    id: 'ca-app-pub-2882779297547171/9275509703',
    isTesting: true,
    autoShow: true
  }

  private interstitial: any

  constructor(
    private admobFree: AdMobFree,
    private constants: Constants
  ) {
  }

  private getbanner(): Promise<any> {
    this.admobFree.banner.config(this.bannerConfig)
    return this.admobFree.banner.prepare()
  }

  private getInterstitial(): Promise<any> {
    this.admobFree.interstitial.config(this.interstitialConfig)
    return this.admobFree.interstitial.prepare()
  }

  public managerBanner() {
    this.getbanner().then(b => {
      console.log(b)
    })
  }

  public managerInterstitial() {
    this.interstitial = setInterval(() => {
      this.getInterstitial().then(b => {
        console.log(b)
      })
    }, this.constants.BANNER_SHOW_TIMER)
  }
  
  public cancelInterstitialInterval() {
    clearInterval(this.interstitial)
  }
}
