import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/observable";
import { catchError } from "rxjs/operators";
import { Constants } from './../../util/constants';
import { ConstantsURL } from '../../util/constants-url';
import Outlay from '../../models/outlay/outlay.model';

@Injectable()
export class OutlayProvider {

  constructor(
    public http: HttpClient,
    private constsUrl: ConstantsURL,
    private consts: Constants
  ) {
  }

  list(token: string, userId: string, page: number): Observable<Outlay[]> {
    return this.http.get<Outlay[]>(`${this.constsUrl.ENDPOINT_OUTLAY_GET}/${userId}/${this.consts.QTT_ITEMS_OUTLAY}/${page}`,
      this.getHeaderParams(token)).pipe(catchError(this.handleError<Outlay[]>('list')))
  }

  create(token: string, outlay: Outlay): Observable<Outlay> {
    return this.http.post<Outlay>(`${this.constsUrl.ENDPOINT_OUTLAY_CREATE}`, outlay, this.getHeaderParams(token)).pipe(catchError(this.handleError<Outlay>('create')))
  }

  private log(msg: string, err: boolean) {
    if (err) console.error('OutlayProvider: ' + msg)
    else console.log('OutlayProvider: ' + msg)
  }

  private handleError<T>(op = 'op', res?: T) {
    return (err: any): Observable<T> => {
      this.log(`${op} failed: ${err.message}`, true)
      throw err
    }
  }

  private getHeaderParams(token: string) {
    let myParams = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'JWT ' + token
    }

    return {
      'headers': new HttpHeaders(myParams)
    }
  }

}
