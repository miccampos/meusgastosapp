import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/observable";
import { catchError } from "rxjs/operators";
import { ConstantsURL } from '../../util/constants-url';
import User from '../../models/user/user.model';
import Auth from '../../models/user/auth.model';

@Injectable()
export class UserProvider {

  constructor(
    public http: HttpClient,
    private consts: ConstantsURL
  ) {
  }

  login(body: User): Observable<Auth> {
    return this.http.post<Auth>(this.consts.ENDPOINT_USER_LOGIN, body, this.getHeaderParams()).pipe(catchError(this.handleError<Auth>('login')))
  }

  private log(msg: string, err: boolean) {
    if (err) console.error('UserProvider: ' + msg)
    else console.log('UserProvider: ' + msg)
  }

  private handleError<T>(op = 'op', res?: T) {
    return (err: any): Observable<T> => {
      this.log(`${op} failed: ${err.message}`, true)
      throw err
    }
  }

  private getHeaderParams() {
    let myParams = {
      'Content-Type': 'application/json; charset=UTF-8',
    }

    return {
      'headers': new HttpHeaders(myParams)
    }
  }

}
